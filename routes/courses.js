//[SECTION] Dependencies and Modules
const auth = require('../auth')
const exp = require("express");
const { send } = require("express/lib/response");
const { verifyAdmin } = require("../auth");
const controller = require('../controllers/courses');

// destructive verify form auth

const {verify} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] [POST] Routes
 route.post('/create', verify, verifyAdmin, (req, res) => { 
     let data = req.body; 
     controller.createCourse(data).then(outcome => {
            res.send(outcome); 
     });
 });

//[SECTION] [GET] Routes
route.get('/all', verify, verifyAdmin, (req, res) => {
    controller.getAllCourse().then(outcome => {
        res.send(outcome)
    })
});

// implement a new route for retrive single course
route.get('/:id', (req, res) => {
    // console.log(req.params.id)
    let courseId = req.params.id;
    controller.getCourse(courseId).then(result => {
        res.send(result);
    });
});

// implement a new route to get all active course
route.get('/', (req, res) => {
    controller.getAllActiveCourse().then(outcome => {
        res.send(outcome);
    });
});

//[SECTION] [PUT] Routes
route.put('/:id', (req, res) => {
    let id = req.params.id;
    let details = req.body;
    
    let cName = details.name;
	let cDesc = details.description;
	let cCost = details.price;

	if (cName !== '' && cDesc !== '' && cCost !== '') {
		controller.updateCourse(id, details).then(outcome => {
            res.send(outcome);
        });
	} else {
		res.send ('incorrect Input')
	}
})
// deactivating a course resourse
route.put('/:id/archive', (req, res) => {
    let courseId = req.params.id;
    controller.deactivateCourse(courseId).then(resultofTheFucntion => {
        res.send(resultofTheFucntion);
    });
});

// Activating a course resourse
route.put('/:id/reactivate', (req, res) => {
    let objectId = req.params.id;
    controller.reactivateCourse(objectId).then(resultofTheFucntion => {
        res.send(resultofTheFucntion);
    });
});


//[SECTION] [DEL] Routes
route.delete('/:id', (req, res) => {

    let id = req.params.id; 
     controller.deleteCourse(id).then(outcome => {
        res.send(outcome);
     });

})
//[SECTION] Export Route System
module.exports = route; 
