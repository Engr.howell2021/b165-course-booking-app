//[SECTION] Dependencies and Modules
const auth = require('../auth')
const exp = require("express"); 
const User = require("../models/User.js");
const controller = require('./../controllers/users.js'); 

// destructive verify form auth

const {verify, verifyAdmin} = auth;
 
//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] Routes-[POST]
route.post('/register', (req, res) => {
    let userDetails = req.body; 
  controller.registerUser(userDetails).then(outcome => {
     res.send(outcome);
  });
});

// LOGIN
route.post("/login", controller.loginUser);

// GET USER DETAILS
route.get("/getUserDetails", verify, controller.getUserDetails)

// ENROLL

route.post('/enroll', verify, controller.enroll);

route.get('/getEnrollments', verify, controller.getEnrollments)



// route.post('/login', (req, res) => {
//    //console.log(req.body);
//    let userDetails = req.body; 
//    //display the input from the user in cli.
//    controller.loginUser(userDetails).then(outcome => {
//       //send the outcome back to the client
//       res.send(outcome);
//    });
// }); 



//[SECTION] Routes-[GET]
//[SECTION] Routes-[PUT]
//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System

module.exports = route; 
