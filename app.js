// [Section] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose")
const dotenv = require("dotenv");
const courseRoutes = require('./routes/courses');
// acquire the routing components for the users collection
const userRoutes = require('./routes/users')

// [Section] Server Setup
const app = express();
dotenv.config(); 
app.use(express.json());
const secret = process.env.CONNECTION_STRING
const port = process.env.PORT

//[SECTION] Application Routes
app.use('/courses',courseRoutes); 
app.use('/users', userRoutes)

// [Section] Database Connect
mongoose.connect(secret);
let dbStatus = mongoose.connection; 
dbStatus.on('open', () => console.log("Database is Connected")); 

// [Section] Gateway Response
app.get('/', (req, res) =>{
    res.send('Welcome to Howell My First Deployment');
});
app.listen(port, () => console.log(`Server is running on port ${port}`))