//[SECTION] Dependencies and Modules
const { findByIdAndRemove, findByIdAndUpdate } = require('../models/Course');
const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
   module.exports.createCourse = (info) => {
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;
     let newCourse = new Course({
     	name: cName,
     	description: cDesc,
     	price: cCost
     }) 
   	 return newCourse.save().then((savedCourse, error) => {
   	 	if (error) {
   	 		return 'Failed to Save New Document';
   	 	} else {
   	 		return savedCourse; 
   	 	}
   	 });
   };

//[SECTION] Functionality [RETRIEVE]
   module.exports.getAllCourse = () => {
		return Course.find({}).then(outcomeNiFind => {
			return outcomeNiFind;
		});
   };

//   Retrieve a single course
   module.exports.getCourse = (id) => {
	   return Course.findById(id).then(resultOfQuery => { 
		   return resultOfQuery
	   });
   }
// Retrieve ActiveCourse
module.exports.getAllActiveCourse = () => {
	return Course.find({isActive: true}).then(resultOfQuery => {
		return resultOfQuery;
	})
}

//[SECTION] Functionality [UPDATE]
module.exports.updateCourse = (id, details) => {

	let cName = details.name;
	let cDesc = details.description;
	let cCost = details.price;
	
	let updatedCourse = {
		name: cName,
		description: cDesc,
		price: cCost
	}
	return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
		if (err) {
			return 'Failed to update Course';
		} else {
			return 'Successfully updated Course';
		}
	})
}
//soft delete methodology
module.exports.deactivateCourse = (id) => {
	let updates = {
		isActive: false
	}	
	return Course.findByIdAndUpdate(id, updates).then((archived, error) => {

		if (archived) {
			return `The Course ${id} has been deactivated` 
		} else {
			return 'Failed to archived course'
		};
	});
};
// Activty reactivate Course
module.exports.reactivateCourse = (id) => {
	let updateCourse = {
		isActive: true
	}	
	return Course.findByIdAndUpdate(id, updateCourse).then((archived, error) => {

		if (archived) {
			return `The Course ${id} has been Activated` 
		} else {
			return 'Failed to archived course'
		};
	});
};


//[SECTION] Functionality [DELETE]
module.exports.deleteCourse = (id) => {
	return Course.findByIdAndRemove(id).then((removedCourse, err) => {
		if (err) {
			return 'No Course was Removed'
		} else {
			return 'Course Successfully Deleted'
		};
	});
};
